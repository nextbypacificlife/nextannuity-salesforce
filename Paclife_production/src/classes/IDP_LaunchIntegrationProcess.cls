global class IDP_LaunchIntegrationProcess {
    private static final integer FIRST_ITEM_INDEX = 0;

    @InvocableMethod
    public static void startIntegrationProcedure(List<DataParam> dataParams) {
        String procedureName;
        Map<String, Object> input;
        Map<String, Object> options = new Map<String, Object>();

        if(dataParams == null || dataParams.isEmpty())
            return;

        try{
            procedureName = dataParams[FIRST_ITEM_INDEX].VlocityIPName;
            input = new Map<String, Object> {
                'Id' => dataParams[FIRST_ITEM_INDEX].Id
            };

            Map<String,Object> ip2Output = new Map<String,Object>();
        
            Object result = (new vlocity_ins.IntegrationProcedureService()).invokeMethod(procedureName, input, ip2Output, options);
            System.debug('--ip2Output: ' + ip2Output);
        }catch(Exception ex){
            throw ex;
        }
    }

    global class DataParam {
        @InvocableVariable(required=true)
        global String vlocityIPName;

        @InvocableVariable(required=true)
        global Id Id;
    }
}