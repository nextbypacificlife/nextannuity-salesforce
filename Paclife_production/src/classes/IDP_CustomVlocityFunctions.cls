global class IDP_CustomVlocityFunctions implements vlocity_ins.VlocityOpenInterface {
  final Integer  FIRST_ITEM_INDEX = 0;
     /*
      inputs - arguments - List<Object> of passed in arguments
      output - result - The result of the Function being called. Supports single Object values, List<Object> or Map<String, Object>
   */
    global Boolean invokeMethod(String methodName, Map<String, Object> inputs, Map<String, Object> output, Map<String, Object> options)
    {   
        System.debug(inputs);
        if (methodName == 'listjoinbykey')
        {
            List<Object> arguments = (List<Object>)inputs.get('arguments');
            System.debug(json.serialize(arguments));
            output.put('result', listjoinbykey(arguments));
        } else if (methodName == 'getlinebreak') {
            output.put('result', getlinebreak());
        } else if (methodName == 'listmergeunique') {
            List<Object> arguments = (List<Object>)inputs.get('arguments');
            try {
              output.put('result', listmergeunique(arguments)); 
              } catch (Exception ex) {
                System.debug(ex.getMessage());
                System.debug(ex.getStackTraceString());
                throw ex;
              }

        }

        System.debug('output: ' + output);
        return true;
    }
/*
    public String getValueByKey(Map<String, Object> input, String key) {
      List<String> keys = key.split(':');
      Object result;
system.debug('keys:'+keys);
      for (String tmpKey : keys) {
        result = ((Map<String, Object>)input).get(tmpKey);
        if (result == null) return null;
      }
      return (String)result;
    }
*/    
    global String getlinebreak() {
        return '\n';
    }

    /* LISTMERGEUNIQUE("uid", "IDP_uid__c",LIST(primary),LIST(new)) */
    global List<Object> listmergeunique(List<Object> arguments) {
      String primaryKey = (String)arguments[0];
      String newKey = (String)arguments[1];
      System.debug(arguments[0]);
      System.debug(arguments[1]);

      List<Object> primaryObjs = (List<Object>)((Map<String,Object>)arguments[2]).get('VLOCITY-FORMULA-LIST');
      List<Object> newObjs = (List<Object>)((Map<String,Object>)arguments[3]).get('VLOCITY-FORMULA-LIST');

      // Map<String, List<Objects> mapPrimaryObjsByKey = new Map<String, List<Objects>();
      Map<String, Map<String, Object>> mapNewObjsByKey = new Map<String, Map<String, Object>>();
      for (Object newObj : newObjs) {
        String newKeyValue = (String)(((Map<String,Object>)newObj).get(newKey));
        System.debug('put key: '+ newKeyValue + ' - ' + newObj);
        mapNewObjsByKey.put(newKeyValue, (Map<String, Object>)newObj);
      }

      for (Object primaryObj : primaryObjs) {
        Map<String, Object> primaryMap = (Map<String, Object>)primaryObj;
        String primaryKeyValue = (String)primaryMap.get(primaryKey);
        // primaryMap.get(primaryKeyValue);
        System.debug('get key: ' + primaryKeyValue + ' - ' + mapNewObjsByKey.get(primaryKeyValue));
        Map<String, Object> newObj = mapNewObjsByKey.get(primaryKeyValue);
        if (newObj != null) {
          primaryMap.putAll(newObj);
        }
      }

      System.debug(arguments[0]);
      System.debug(arguments[1]);

       System.debug(primaryObjs);
      return primaryObjs;
    }

    /* LISTJOINBYKEY(LIST(IGOStatus:trueRules),"ruleDetails:message","\n") */
    global String listjoinbykey(List<Object> arguments) {
      try {
        String key = (String)arguments[1];

        List<String> stringsToJoin = getMessages(arguments[FIRST_ITEM_INDEX]);
/*
system.debug('key:'+key);

        Set<String> stringsToJoin = new Set<String>();
        List<Object> objs = (List<Object>)((Map<String,Object>)arguments[0]).get('VLOCITY-FORMULA-LIST');
        System.debug(arguments[0]);
        System.debug(arguments[1]);
        for (Object obj : objs) {
          System.debug(obj);
          Map<String, Object> input = (Map<String, Object>)obj;
          String str = getValueByKey(input, key);
          if (str != null) {
            stringsToJoin.add(str);
          }
        }
        System.debug((String)arguments[2]);
        System.debug('\n');
        System.debug('Test\nTest1');
*/
          return String.join(stringsToJoin, ((String)arguments[2]).replace('\\n','\n'));
        
        } catch (Exception ex) {
          System.debug(LoggingLevel.ERROR, ex);
          return ex.getMessage() + '\n' + ex.getStackTraceString();
        }
    }

    global List<String> getMessages(Object arg) {
      Set<String> ret = new Set<String>();

      String argJson = Json.serialize(arg);

      JSONParser parser = JSON.createParser(argJson);

      JSONToken token = parser.nextToken();
      while(token != null){
          if(token == System.JSONToken.FIELD_NAME && parser.getCurrentName() == 'message') {
              parser.nextToken();
              ret.add(parser.getText());
          }
          token = parser.nextToken();
      }
      return new List<String>(ret);
    }
}