@isTest
public class PL_paymentControllerExtensionTest {
    
    @testSetup
    static void DataSetup() {
        
        //Assign record type map
        Map<String,Id> recordTypeMap = new Map<String,Id>();
        for(RecordType rt : [select Id, SobjectType, DeveloperName from RecordType where DeveloperName in ('PersonAccount','IndividualAnnuity')]){
            recordTypeMap.put(rt.SobjectType+'||'+rt.DeveloperName,rt.Id);
        }
        
        //Account
        List<Account> accountInsertList = new List<Account>();
        for (Integer i=0; i<2; i++) {
            accountInsertList.add(new Account(
                RecordTypeId=recordTypeMap.get('Account||PersonAccount'),
                LastName='TEST Account '+String.valueOf(i),vlocity_ins__SocialSecurityNumber__pc = '123-45-678'+String.valueOf(i)
            ));
        }
        insert accountInsertList;
                
        //Asset/Policy
        List<Asset> assetInsertList = new List<Asset>();
        for (Integer i=0; i<2; i++) {
            assetInsertList.add(new Asset(
                RecordTypeId=recordTypeMap.get('Asset||IndividualAnnuity'),
                Name='TEST Policy '+String.valueOf(i),
                AccountId=accountInsertList[Math.mod(i,5)].Id,
                PL_Policy_Number__c='PLN'+String.valueOf(i*13+100), //unique while decidedly different from the name values
                IDP_Status__c='Application Upload', 
                Status='Application',
                Policy_Issue_Date__c=Date.Today() 
            ));
        }
        insert assetInsertList;
        
    }
    
    @isTest
    static void savepayment() {
        
        List<Asset> astlst=new List<Asset>();
        astlst=[SELECT id ,AccountId FROM Asset];
        
        List<IDP_Payment__c> plpaymentlst=new List<IDP_Payment__c>();
        List<vlocity_ins__PaymentMethod__c> paylst=new List<vlocity_ins__PaymentMethod__c>(); 
        
        for(Asset ast:astlst){
           vlocity_ins__PaymentMethod__c paym=new vlocity_ins__PaymentMethod__c();
           paym.vlocity_ins__AccountId__c=ast.AccountId;
           paym.vlocity_ins__MethodType__c='Bank Account';
           paym.vlocity_ins__BankAccountType__c='Savings';
           paym.vlocity_ins__BankRoutingNumber__c='464575656';
           paym.vlocity_ins__BankAccountNumber__c='0987654321';
           paym.vlocity_ins__IsActive__c=true;
           paylst.add(paym);
        } 
       
        insert paylst;
        
        Integer i=0;
         for(Asset ast:astlst){   
           IDP_Payment__c idppay=new IDP_Payment__c();
           idppay.Contract__c=ast.Id;
           idppay.Initial_Payment_Amount__c=10;
           idppay.Modal_Annuity_Payout__c=1;
           idppay.Payor__c=ast.AccountId;
           idppay.Payment_Method__c=paylst[i].Id;
           if(i==1)
             idppay.Status__c = 'Successful';
           plpaymentlst.add(idppay);
           i++;
        }
        insert plpaymentlst;
    
        ApexPages.StandardController sc = new ApexPages.StandardController(plpaymentlst[0]);
        paymentControllerExtension  plpaymnt= new paymentControllerExtension (sc);
        plpaymnt.save();
        
        
        ApexPages.StandardController sc2 = new ApexPages.StandardController(plpaymentlst[1]);
        paymentControllerExtension  plpaymnt2= new paymentControllerExtension (sc2);
        plpaymnt2.save();
    }
}