public class ContentDocumentLinkHandler {

    //Constants
    public static final Set<String> affectedObjectSet = new Set<String>{'Asset'}; //Designed to support adding more affected objects in the future
    public static final Set<String> omittedFileTypeSet = new Set<String>{'SNOTE'}; //Designed to support adding more omitted file types
    public static final String defaultDocumentType = 'Contract Packet';
    public static final String defaultDocumentSource = 'NEXT Platform';
    public static final String defaultEventType = 'Document_Ready';
    public static final String fileTagPrefix = '[DRAFT]';
    
    //Variables
    public static Boolean run = true; 
    public static Map<Id,ContentDocument> contentDocumentMap = new Map<Id,ContentDocument>();
    public static Map<Id,Asset> assetMap = new Map<Id,Asset>(); //Parent Asset when parent is Asset
    public static List<IDP_Document__c> idpDocInsertList = new List<IDP_Document__c>();
    public static List<ContentDocumentLink> cdlInsertList = new List<ContentDocumentLink>();
    public static Set<id> idpDocIdSet = new Set<Id>();
    public static List<Database.SaveResult> publishResultList = new List<Database.SaveResult>();
    public static Map<Id,Id> asset_idvscdl_idMap = new Map<Id,Id>();
    public static List<Task> tasklist=new List<Task>();
        
    //Methods
    public static void MoveToCustomRepository(List<ContentDocumentLink> triggerNewList){
        if(!(run && PLUtilities.getIsTriggerEnabled('ContentDocumentLinkTrigger'))){
            return;
        }
        
        //Convert Set of included object names to a Set of key prefixes
        Set<String> sourceObjectTypeSet = PLUtilities.getObjectTypeSet(affectedObjectSet);
        
        //Put all parent ContentDocument IDs in a set for use in the next query
        Set<Id> contentDocumentIdFullSet = new Set<Id>(); 
        for (ContentDocumentLink cdl : triggerNewList) {
            contentDocumentIdFullSet.add(cdl.ContentDocumentId);
            asset_idvscdl_idMap.put(cdl.LinkedEntityId,cdl.Id);
        }
        
        //Get the parent ContentDocument in order to access its filetype
        //and for purposes of adding some information to the custom Document record
        contentDocumentMap.putAll(new Map<Id,ContentDocument>([select id, Title, FileType From ContentDocument where Id in :contentDocumentIdFullSet]));
        
        //Evaluate the parent. Continue logic for each Trigger record whose LinkedEntityId is an object type in the Custom Metadata set. Else, avoid logic.
        //Build list of records that need to be acted upon
        List<ContentDocumentLink> handleCDLList = new List<ContentDocumentLink>();
        Set<Id> contentDocumentIdSet = new Set<Id>(); //Set of ContentDocumentId, for use in map generation later
        Set<Id> linkedEntityIdSet = new Set<Id>(); //Set of LinkedEntityId, for use in map generation 
        for(ContentDocumentLink cdl : triggerNewList) {
            String cdlString = (String) cdl.LinkedEntityId;
            
            if(sourceObjectTypeSet.contains(cdlString.substring(0,3)) //Compare using first three characters of the record ID, which is the object ID
               && !omittedFileTypeSet.contains(contentDocumentMap.get(cdl.ContentDocumentId).FileType) ){ 
                   handleCDLList.add(cdl);
                   contentDocumentIdSet.add(cdl.ContentDocumentId); 
                   linkedEntityIdSet.add(cdl.LinkedEntityId);
               }
        }
        
        //Act upon only those records that match the relevant document types, as filtered above
        if (handleCDLList.size()>0) {
            
            //Get parent Asset info for purposes of adding some info to the custom Document record            
            assetMap.putAll(new Map<Id,Asset>([select id, PL_Policy_Number__c From Asset where Id in :linkedEntityIdSet]));
            
            //List of current CDL Ids for later deletion
            List<Id> deleteCDLIdList = new List<Id>();
            
            //Open database transaction and try/catch block, to handle only the custom Document insertion and its ContentDocumentLink record
            Savepoint transactionSavePoint = Database.setSavepoint();
            Id latestCdlId;
            try {
                
                //Map of Share Types for use later. 
                Map<String,String> contentDocShareTypeMap = new Map<String,String>();
                
                //Map of Content Doc IDs for use later.
                Map<String,Id> contentDocIdMap = new Map<String,Id>();
                
                //Build custom Document record list attached to the same parent record as the Trigger record's parent's parent
                for(ContentDocumentLink cdl : handleCDLList) {  
                    //Set variable outside try/catch context to know which caused an exception in the catch block
                    latestCdlId = cdl.Id;
                    
                    //Get policy number while handling map nulls
                    String idpDocPolicyNumber;
                    if(assetMap.get(cdl.LinkedEntityId) != null) {
                        idpDocPolicyNumber = assetMap.get(cdl.LinkedEntityId).PL_Policy_Number__c;
                    }
                    
                    //Set up document record
                    IDP_Document__c idpd = new IDP_Document__c(
                        Name=contentDocumentMap.get(cdl.ContentDocumentId).Title, 
                        Policy__c=cdl.LinkedEntityId,
                        Document_Type__c=defaultDocumentType, 
                        Source__c=defaultDocumentSource, 
                        Policy_Number__c=idpDocPolicyNumber
                    );
                    idpDocInsertList.add(idpd);
                    
                    contentDocShareTypeMap.put(idpd.Policy_Number__c, cdl.ShareType); //Hopefully this is a unique number by ContentDocument and by Asset; if not, we'll need to use a new External ID
                    contentDocIdMap.put(idpd.Policy_Number__c, cdl.ContentDocumentId); //Ditto
                    
                    //For deletion
                    deleteCDLIdList.add(cdl.Id);
                }
                
                //Insert custom document records
                If (idpDocInsertList.size()>0){
                    insert idpDocInsertList;                    
                    
                    //Requery to get data for child records and build the list
                    for(IDP_Document__c idpd : [select id, Policy_Number__c from IDP_Document__c where id in :idpDocInsertList]){
                        //Build ContentDocumentLink record list attached to the new custom Document record
                        cdlInsertList.add(new ContentDocumentLink(
                            ContentDocumentId=contentDocIdMap.get(idpd.Policy_Number__c),
                            LinkedEntityId=idpd.Id,
                            ShareType=contentDocShareTypeMap.get(idpd.Policy_Number__c)
                        )); 
                        
                        //Generate Set of IDP Doc Ids for use in another method
                        idpDocIdSet.add(idpd.id);
                    }
                    
                    //Insert
                    insert cdlInsertList;
                    
                    PublishToContractEvent();
                }
                                
            } catch (Exception e) {
                //Roll back savepoint from inner try/catch block  
                Database.rollback(transactionSavePoint); 
                
                //Log error
                System.debug('File failed. '+e.getStackTraceString());
                
                Trigger.newMap.get(latestCdlId).addError('File failed. ');//+e.getLineNumber()
            }
            
            //Delete the current Trigger record insert 
            DeleteCDL(deleteCDLIDList);
            
        } //END IF handleCDLList has records
        
    } 
    @future
    public static void DeleteCDL(List<Id> deleteCDLIDList) {
        List<ContentDocumentLink> deleteCDLList = new List<ContentDocumentLink>();
        for(Id cdlId : deleteCDLIDList){
            deleteCDLList.add(new ContentDocumentLink(Id=cdlId));
        }
        delete deleteCDLList; 
    }
    
    private static void PublishToContractEvent() {
        //Currently this method is only intended to be called from the main method in this handler, due to class variable setup

        //Get contentdocument information 
        List<ContentDocumentLink> publishCDLList = [select id, LinkedEntityId, ContentDocumentId, ContentDocument.LatestPublishedVersionId, ContentDocument.FileType, ContentDocument.Title from ContentDocumentLink where LinkedEntityId in :idpDocIdSet];     
        Map<Id,ContentDocumentLink> idpDocumentIdToCDLObjectMap = new Map<Id,ContentDocumentLink>();
        for(ContentDocumentLink cdl : publishCDLList) {
            idpDocumentIdToCDLObjectMap.put(cdl.LinkedEntityId, cdl);
        }
        
        //Evaluate the file name and only process if it starts with fileTagPrefix
        Set<Id> processIDPDocumentIdSet = new Set<Id>();
        for(IDP_Document__c idpdoc : idpDocInsertList) {
            if(idpdoc.Name.substring(0,fileTagPrefix.length()) == fileTagPrefix ){ 
                processIDPDocumentIdSet.add(idpdoc.Id);
            }
        }

        //Get parent Asset data related to IDP_Document__c
        List<IDP_Document__c> handleDocumentList = [select id, Name, Policy__c, Policy__r.PL_Policy_Number__c, Policy__r.Integration_Id__c, Policy__r.IDP_ExternalPolicyId__c, Document_Type__c, Source__c
                                                    from IDP_Document__c
                                                    where id in :processIDPDocumentIdSet];
        
        Set<Id> policyIdSet = new Set<Id>();
        Set<Id> policyIdSetwithpay = new Set<Id>();
        Set<Id> policyIdSetwithoutpay = new Set<Id>();
        
        if(handleDocumentList.size()>0) {
            for(IDP_Document__c idpdocs : handleDocumentList)
                policyIdSet.add(idpdocs.policy__c);
            
            if(policyIdSet.size()>0){
               //Get IDP Payment records related to Assets(Parent to IDP document)
                List<IDP_Payment__c> idppaymentList=new List<IDP_Payment__c>();
                idppaymentList=[SELECT Name,Initial_Payment_Amount__c,Modal_Annuity_Payout__c,Contract__c,CreatedById 
                                FROM IDP_Payment__c
                                WHERE Contract__c IN:policyIdSet ORDER BY Effective_date_of_payment__c ASC];
                
                //GET First IDP payment order by effective date with and without annuity payout > 0
                //System.debug(idppaymentList);
                for(IDP_Payment__c idpay:idppaymentList){
                    if(!policyIdSetwithpay.contains(idpay.Contract__c) && !policyIdSetwithoutpay.contains(idpay.Contract__c) ){
                        if(idpay.Initial_Payment_Amount__c!=null && idpay.Modal_Annuity_Payout__c!=null && idpay.Modal_Annuity_Payout__c>0){
                           policyIdSetwithpay.add(idpay.Contract__c);
                        }else{
                           policyIdSetwithoutpay.add(idpay.Contract__c);
                           Task t = new Task();
                           t.OwnerId = idpay.CreatedById;
                           t.Subject = 'Modal Annuity Payout should not be less than zero in '+idpay.Name;
                           t.Status = 'Open';
                           t.Priority = 'Normal';
                           t.WhatId = idpay.Contract__c;
                           tasklist.add(t);
                        }
                    }
                }
                if(tasklist.size()>0)
                   insert tasklist;
            }
            
            //Publish to platform event and set up update lists to remove temporary tag from file name
            List<Contract_Document_Event__e> insertContractDocumentEventList = new List<Contract_Document_Event__e>();
            List<IDP_Document__c> updateIdpDocumentList = new List<IDP_Document__c>();
            List<ContentDocument> updateContentDocumentList = new List<ContentDocument>();
            // Document list to show error 
            List<IDP_Document__c> errorDocumentList = new List<IDP_Document__c>();
            
            //System.debug(handleDocumentList);
            for(IDP_Document__c idpdoc : handleDocumentList) {
               //Record should only be included IF the associated asset has an initial payment with a modal payment payout>0
                if(policyIdSetwithpay.size()>0 && policyIdSetwithpay.contains(idpdoc.Policy__c)){
                    ContentDocumentLink cdl = idpDocumentIdToCDLObjectMap.get(idpdoc.Id);
                    //System.debug(cdl);
                    //TODO: Handle business error when a required field is null 
                    insertContractDocumentEventList.add(new Contract_Document_Event__e(
                        Content_Version_Id__c=cdl.ContentDocument.LatestPublishedVersionId,	//Required
                        Contract_Id__c=idpdoc.Policy__c, 									//Optional
                        Contract_Number__c=idpdoc.Policy__r.PL_Policy_Number__c, 			//Required
                        Event_Type__c=defaultEventType, 									//Optional
                        Document_Type__c=idpdoc.Document_Type__c, 							//Required
                        Integration_Id__c=idpdoc.Policy__r.Integration_Id__c, 				//Required
                        File_Type__c=cdl.ContentDocument.FileType, 							//Required
                        Title__c=cdl.ContentDocument.Title.replace(fileTagPrefix,''), 		//Required
                        Source__c=idpdoc.Source__c, 										//Required
                        IDP_ExternalPolicyId__c=idpdoc.Policy__r.IDP_ExternalPolicyId__c	//Required
                    ));
                    System.debug('insertContractDocumentEventList='+insertContractDocumentEventList);
                    
                    updateIdpDocumentList.add(new IDP_Document__c(
                        Id=idpdoc.Id,
                        Name=idpdoc.Name.replace(fileTagPrefix,'')
                    ));
                    
                    updateContentDocumentList.add(new ContentDocument(
                        Id=cdl.ContentDocumentId,
                        Title=idpdoc.Name.replace(fileTagPrefix,'')
                    ));
                }
                else if(policyIdSetwithoutpay.size()>0 && policyIdSetwithoutpay.contains(idpdoc.Policy__c)){
                        errorDocumentList.add(idpdoc); // Document list for adding error 
                }
            }

            if(insertContractDocumentEventList.size()>0)
               publishResultList = EventBus.publish(insertContractDocumentEventList);
            if(updateIdpDocumentList.size()>0)
               List<Database.SaveResult> updateIDPDocumentResultList = Database.update(updateIdpDocumentList);
            if(updateContentDocumentList.size()>0)
               List<Database.SaveResult> updateContentDocumentResultList = Database.update(updateContentDocumentList);
            //Skipping error handling for now until such time as a log exists or if it is determined we want to show an error to the user
            
            //for(IDP_Document__c erridpdoc : errorDocumentList){
            //   Trigger.newMap.get(asset_idvscdl_idMap.get(erridpdoc.Policy__c)).addError('File failed: Modal Annuity Payout should not be less than zero in PL Payments.');
            //}
            if(errorDocumentList.size()>0)
               delete errorDocumentList;
        }
        
    }
    
    @InvocableMethod(label='Publish Document Event' description='Publish a Contract Document Event for all attachments per given IDP Document record.')
    //TODO: This method is for ad hoc testing and is not required for the main functionality; can be deleted/commented out before production release
    public static void PublishDocumentEvent(List<Id> IDPDocumentIdList) {
        Map<Id,IDP_Document__c> idpDocMap = new Map<Id,IDP_Document__c>([select Policy__c, Policy__r.PL_Policy_Number__c, Document_Type__c, Policy__r.Integration_Id__c, Policy__r.IDP_ExternalPolicyId__c, Source__c
                                  from IDP_Document__c 
                                 where ID in :IDPDocumentIdList]);
        List<ContentDocumentLink> cdlList = [select Id, LinkedEntityId, ContentDocument.LatestPublishedVersionId, ContentDocument.FileType, ContentDocument.Title
                                   from ContentDocumentLink 
                                   where LinkedEntityId in :IDPDocumentIdList];
        List<Contract_Document_Event__e> insertContractDocumentEventList = new List<Contract_Document_Event__e>();
        for(ContentDocumentLink cdl : cdlList) {
            IDP_Document__c idpdoc = idpDocMap.get(cdl.LinkedEntityId);
            insertContractDocumentEventList.add(new Contract_Document_Event__e(
                Content_Version_Id__c=cdl.ContentDocument.LatestPublishedVersionId, //Required
                Contract_Id__c=idpdoc.Policy__c, //Optional
                Contract_Number__c=idpdoc.Policy__r.PL_Policy_Number__c, //Required
                Event_Type__c=ContentDocumentLinkHandler.defaultEventType, //Optional
                Document_Type__c=idpdoc.Document_Type__c, //Required
                Integration_Id__c=idpdoc.Policy__r.Integration_Id__c, //Required
                File_Type__c=cdl.ContentDocument.FileType, //Required
                Title__c=cdl.ContentDocument.Title, //Required
                Source__c=idpdoc.Source__c, //Required
                IDP_ExternalPolicyId__c=idpdoc.Policy__r.IDP_ExternalPolicyId__c //Required
            ));
        }
        if(insertContractDocumentEventList.size()>0) {
            publishResultList = EventBus.publish(insertContractDocumentEventList);        
        }
    }
}