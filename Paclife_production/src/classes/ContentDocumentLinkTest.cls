@isTest
public class ContentDocumentLinkTest {


    @testSetup
    static void DataSetup() {
        
        //Assign record type map
        Map<String,Id> recordTypeMap = new Map<String,Id>();
        for(RecordType rt : [select Id, SobjectType, DeveloperName from RecordType where DeveloperName in ('PersonAccount','IndividualAnnuity')]){
            recordTypeMap.put(rt.SobjectType+'||'+rt.DeveloperName,rt.Id);
        }
        
        //Account
        List<Account> accountInsertList = new List<Account>();
        for (Integer i=0; i<5; i++) {
            accountInsertList.add(new Account(
                RecordTypeId=recordTypeMap.get('Account||PersonAccount'),
                LastName='TEST Account '+String.valueOf(i),vlocity_ins__SocialSecurityNumber__pc = '123-45-678'+String.valueOf(i)
            ));
        }
        insert accountInsertList;
        
        //Product
        Product2 productInsert = new Product2(
            Name='Test Product',
            Family='Test Annuity',
            IDP_Product_Version__c=2
        );
        insert productInsert;
        System.assert(productInsert.Id != null);
        
        //Asset/Policy
        List<Asset> assetInsertList = new List<Asset>();
        for (Integer i=0; i<10; i++) {
            assetInsertList.add(new Asset(
                RecordTypeId=recordTypeMap.get('Asset||IndividualAnnuity'),
                Name='TEST Policy '+String.valueOf(i),
                AccountId=accountInsertList[Math.mod(i,5)].Id,
                PL_Policy_Number__c='PLN'+String.valueOf(i*13+100), //unique while decidedly different from the name values
                IDP_Status__c='Application Upload', 
                Status='Application',
                Policy_Issue_Date__c=Date.Today(),
                Product2Id=productInsert.Id
            ));
        }
        insert assetInsertList;
        
        List<IDP_Payment__c> plpaymentlst=new List<IDP_Payment__c>();
        List<vlocity_ins__PaymentMethod__c> paylst=new List<vlocity_ins__PaymentMethod__c>(); 
        
        for(Asset ast:assetInsertList){
           vlocity_ins__PaymentMethod__c paym=new vlocity_ins__PaymentMethod__c();
           paym.vlocity_ins__AccountId__c=ast.AccountId;
           paym.vlocity_ins__MethodType__c='Bank Account';
           paym.vlocity_ins__BankAccountType__c='Savings';
           paym.vlocity_ins__BankRoutingNumber__c='464575656';
           paym.vlocity_ins__BankAccountNumber__c='0987654321';
           paym.vlocity_ins__IsActive__c=true;
           paylst.add(paym);
        } 
       
        insert paylst;
        
        Integer i=0;
         for(Asset ast:assetInsertList){   
           IDP_Payment__c idppay=new IDP_Payment__c();
           idppay.Contract__c=ast.Id;
           idppay.Initial_Payment_Amount__c=10;
           idppay.Modal_Annuity_Payout__c=1;
           idppay.Payor__c=ast.AccountId;
           idppay.Payment_Method__c=paylst[i].Id;
           plpaymentlst.add(idppay);
           i++;
        }
        insert plpaymentlst;
    }
    
    @isTest
    static void CDLTrigger() {
        List<Account> testAccountList = [select Id, LastName from Account];
        
        List<Asset> testAssetList = [select Id, Name from Asset];
        
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        Map<String,Id> titleToEntityIdMap = new Map<String,Id>();
        String titleName;
        for(Asset ast : testAssetList){
            titleName = '[DRAFT]TEST Doc '+ast.Name;
            contentVersionList.add(new ContentVersion(
                Title = titleName,
                PathOnClient = 'TESTDoc'+ast.Name+'.pdf',
                VersionData = Blob.valueOf('Test Content '+ast.Name),
                IsMajorVersion = true
            ));
            titleToEntityIdMap.put(titleName,ast.Id);
        }
        //Add one to be attached to the Account directly, as a negative test
        titleName='TEST Doc Account 0';
        contentVersionList.add(new ContentVersion(
            Title = titleName,
            PathOnClient = 'TESTDocAccount.pdf',
            VersionData = Blob.valueOf('Test Content Account'),
            IsMajorVersion = true
        ));
        titleToEntityIdMap.put(titleName,testAccountList[0].Id);

        insert contentVersionList;    
        List<Id> docidlst=new List<Id>();
        List<ContentDocument> documentList = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        Set<Id> documentIdSet = new Set<Id>();
        List<ContentDocumentLink> cdlInsertList = new List<ContentDocumentLink>();
        For(ContentDocument cd : documentList){
            cdlInsertList.add(new ContentDocumentLink(
                LinkedEntityId = titleToEntityIdMap.get(cd.Title),
                ContentDocumentId = cd.Id,
                shareType = 'V'
            ));
            
            documentIdSet.add(cd.Id);
        }

        Test.startTest();
        insert cdlInsertList;
        Test.stopTest();

        //Required Data for Assertions
        //Account->Document
        //Account->Asset(Policy Number=not blank)->Document
        
        //Requery Data for Assertions
        List<IDP_Document__c> testIDPDocumentList = [select Id, Name, Policy__c, Policy_Number__c, Policy__r.PL_Policy_Number__c, Source__c, Document_Type__c
                                                     from IDP_Document__c];
        List<ContentDocument> testContentDocumentList = [select Id, Title from ContentDocument];
        List<ContentDocumentLink> testCDLList = [select Id, ContentDocumentId, ContentDocument.Title, LinkedEntityId 
                                                 from ContentDocumentLink 
                                                 where ContentDocumentId in :documentIdSet
                                                 and LinkedEntityId != :UserInfo.getUserId()
                                                ];
        System.assertNotEquals(0, testCDLList.size()); //Found Records
         
        Map<String,Id> testDocTitleToEntityIdMap = new Map<String,Id>();
        Map<Id,String> testEntityIdToDocTitleMap = new Map<Id,String>();
        for(ContentDocumentLink cdl : testCDLList) {
            System.debug(cdl.LinkedEntityId +','+ cdl.ContentDocument.Title);
            testDocTitleToEntityIdMap.put(cdl.ContentDocument.Title,cdl.LinkedEntityId);
            testEntityIdToDocTitleMap.put(cdl.LinkedEntityId,cdl.ContentDocument.Title);
         
        }
        
        //Assertions
        
        //File attached to non-Asset is not moved and no custom Document record is created for it
        //Meaning: ContentDocumentLink.LinkedEntityId = Account, and TODO: same ContentDocumentId does not have a ContentDocumentLink record with LinkedEntityId = IDP Document
        System.assertEquals('001', String.valueOf(testDocTitleToEntityIdMap.get('TEST Doc Account 0')).substring(0,3)); 
        //Remove this one so that the rest can be tested
        testDocTitleToEntityIdMap.remove('TEST Doc Account 0');
        
        //Get prefixes from object names
        Set<String> sourceObjectTypeSet = PLUtilities.getObjectTypeSet(new Set<String>{'IDP_Document__c'});
        PLUtilities.putInObjectTypeMap('IDP_Document__c');
        
        //File attached to Asset is moved to a custom Document record. 
        for(String testTitleName : testDocTitleToEntityIdMap.keySet()) {
            System.assertEquals(PLUtilities.objectTypeMap.get('IDP_Document__c'), String.valueOf(testDocTitleToEntityIdMap.get(testTitleName)).substring(0,3)); 
        }
        
        //Succeeding Records:
        for(IDP_Document__c testIdpdoc : testIDPDocumentList){
            docidlst.add(testIdpdoc.Id);
            //IDP_Document__c.Policy_Number__c = (Policy)Asset.Policy_Number__c
            System.assertEquals(testIdpdoc.Policy__r.PL_Policy_Number__c, testIdpdoc.Policy_Number__c);
            
            //IDP_Document__c.Name = ContentVersion.Name
            //For now, using ContentDocument.Title; can change if needed
            System.assertEquals(testEntityIdToDocTitleMap.get(testIdpdoc.Id) , testIdpdoc.Name); 
            
            System.assertEquals(ContentDocumentLinkHandler.defaultDocumentSource,testIdpdoc.Source__c);
            
            System.assertEquals(ContentDocumentLinkHandler.defaultDocumentType,testIdpdoc.Document_Type__c);
                        
            //The complete contract package related to the (Policy)Asset should be updated to look up to the newly created IDP_Document__c record.
            //No assertion needed? Due to Master/Detail relationship of Asset to IDP Document

        }
        ContentDocumentLinkHandler.PublishDocumentEvent(docidlst);
        //Test Event Publish
        System.assertNotEquals(0, ContentDocumentLinkHandler.publishResultList.size());
        for(Database.SaveResult sr : ContentDocumentLinkHandler.publishResultList){
            System.assert(sr.isSuccess());
        }
        
        
    }
    
    @isTest
    static void getObjectNegativeTest(){
        sObject testNullObject = plUtilities.getObject(null); 
        System.assertEquals(null, testNullObject);
    }
    
    @isTest
    static void CDLNegativeTest() {
        
        //Assign record type map
        Map<String,Id> recordTypeMap = new Map<String,Id>();
        for(RecordType rt : [select Id, SobjectType, DeveloperName from RecordType where DeveloperName in ('PersonAccount','IndividualAnnuity')]){
            recordTypeMap.put(rt.SobjectType+'||'+rt.DeveloperName,rt.Id);
        }

        //Get test setup data
        List<Account> testAccountList = [select Id, LastName from Account LIMIT 5];
        List<Product2> testProductList = [select Id, Name from Product2 LIMIT 1];
        
        //Create individual scenario assets
        List<Asset> testAssetList = new List<Asset>();
        
        //Scenario 1. Asset with blank policy number
        testAssetList.add(new Asset(
            RecordTypeId=recordTypeMap.get('Asset||IndividualAnnuity'),
            Name='TEST Policy Scenario 1',
            AccountId=testAccountList[1-1].Id,
            PL_Policy_Number__c=null,
            IDP_Status__c='Application Upload', 
            Status='Application',
            Product2Id=testProductList[0].Id
        ));

        //Scenario X: Null content document title
        
        //Scenario 2-3: Two Assets with same policy number: possible? if so, handle if possible 
        testAssetList.add(new Asset(
            RecordTypeId=recordTypeMap.get('Asset||IndividualAnnuity'),
            Name='TEST Policy Scenario 2',
            AccountId=testAccountList[2-1].Id,
            PL_Policy_Number__c='PLNScenario2to3',
            IDP_Status__c='Application Upload', 
            Status='Application',
            Product2Id=testProductList[0].Id 
        ));
        testAssetList.add(new Asset(
            RecordTypeId=recordTypeMap.get('Asset||IndividualAnnuity'),
            Name='TEST Policy Scenario 3',
            AccountId=testAccountList[3-1].Id,
            PL_Policy_Number__c='PLNScenario2to3',
            IDP_Status__c='Application Upload', 
            Status='Application',
            Product2Id=testProductList[0].Id 
        ));

        insert testAssetList;
        
        
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        Map<String,Id> titleToEntityIdMap = new Map<String,Id>();
        String titleName;
        
        //Scenario 1. Asset with blank policy number
        Asset ast = testAssetList[1-1]; //is this reliable, by index? or do we have to put in a map?
        titleName = 'TEST Doc '+ast.Name;
        contentVersionList.add(new ContentVersion(
            Title = titleName,
            PathOnClient = 'TESTDoc'+ast.Name+'.pdf',
            VersionData = Blob.valueOf('Test Content '+ast.Name),
            IsMajorVersion = true
        ));        
        titleToEntityIdMap.put(titleName,ast.Id);

        //Scenario X: Null content document title
        //This fails in standard validation, as it causes a blank LinkedEntityId
        /*ast = testAssetList[2-1];
        titleName = null;
        contentVersionList.add(new ContentVersion(
            Title = titleName,
            PathOnClient = 'TESTDoc'+ast.Name+'.pdf',
            VersionData = Blob.valueOf('Test Content '+ast.Name),
            IsMajorVersion = true
        ));        
        titleToEntityIdMap.put(titleName,ast.Id); //not sure this will work, due to null key
        */
        
        //Scenario 2-3: Two Assets with same policy number: possible? if so, handle if possible 
        ast = testAssetList[2-1];
        titleName = 'TEST Doc '+ast.Name;
        contentVersionList.add(new ContentVersion(
            Title = titleName,
            PathOnClient = 'TESTDoc'+ast.Name+'.pdf',
            VersionData = Blob.valueOf('Test Content '+ast.Name),
            IsMajorVersion = true
        ));        
        titleToEntityIdMap.put(titleName,ast.Id); 
        ast = testAssetList[3-1];
        titleName = 'TEST Doc '+ast.Name;
        contentVersionList.add(new ContentVersion(
            Title = titleName,
            PathOnClient = 'TESTDoc'+ast.Name+'.pdf',
            VersionData = Blob.valueOf('Test Content '+ast.Name),
            IsMajorVersion = true
        ));        
        titleToEntityIdMap.put(titleName,ast.Id); 

        
        insert contentVersionList;    
        
        //Prepare and Insert ContentDocumentLink Records
        List<ContentDocument> documentList = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        Set<Id> documentIdSet = new Set<Id>();
        List<ContentDocumentLink> cdlInsertList = new List<ContentDocumentLink>();
        For(ContentDocument cd : documentList){
            cdlInsertList.add(new ContentDocumentLink(
                LinkedEntityId = titleToEntityIdMap.get(cd.Title),
                ContentDocumentId = cd.Id,
                shareType = 'V'
            ));
            
            documentIdSet.add(cd.Id);
        }
        
        System.debug(cdlInsertList);
        
        Test.startTest();
        insert cdlInsertList;
        Test.stopTest();

        //Required Data for Assertions
        //Account->Asset(Policy Number=blank)->Document
        //Account->Asset(Policy Number=not blank)->Document(Title=blank)
        //Account->Asset(Policy Number=duplicate)->Document

        //Requery Data for Assertions
        List<IDP_Document__c> testIDPDocumentList = [select Id, Name, Policy__c, Policy_Number__c, Policy__r.PL_Policy_Number__c, Source__c, Document_Type__c
                                                     from IDP_Document__c];
        List<ContentDocument> testContentDocumentList = [select Id, Title from ContentDocument];
        List<ContentDocumentLink> testCDLList = [select Id, ContentDocumentId, ContentDocument.Title, LinkedEntityId 
                                                 from ContentDocumentLink 
                                                 where ContentDocumentId in :documentIdSet
                                                 and LinkedEntityId != :UserInfo.getUserId()
                                                ];
        
        //Scenario 1: Policy without policy number: determine expected behavior
        

        //Scenario X: Null content document title: doesn't work
        //Scenario 2-3: Two Assets with same policy number: determine how to handle  
        //Scenario 4: Attempt causing an error in the inner block
        //Scenario 5: Attempt causing an error in the outer block
        
        //Exception test
        //ContentDocumentLinkHandler.throwError=true;
        //ContentDocumentLinkHandler.MoveToCustomRepository(testCDLList);

    }
    
    @isTest
    static void TriggerRunVariableTest() {
        Account testAccount = [select Id, LastName from Account limit 1];
        
        Asset ast = [select Id, Name from Asset where AccountId=:testAccount.Id limit 1];
        
        Map<String,Id> titleToEntityIdMap = new Map<String,Id>();
        String titleName;
        titleName = 'TEST Doc '+ast.Name;
        ContentVersion insertContentVersion = new ContentVersion(
            Title = titleName,
            PathOnClient = 'TESTDoc'+ast.Name+'.pdf',
            VersionData = Blob.valueOf('Test Content '+ast.Name),
            IsMajorVersion = true
        );
        titleToEntityIdMap.put(titleName,ast.Id);

        insert insertContentVersion;    
        
        ContentDocument cd = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument limit 1]; //should be 1 anyway
        ContentDocumentLink cdlInsert = new ContentDocumentLink(
                LinkedEntityId = titleToEntityIdMap.get(cd.Title),
                ContentDocumentId = cd.Id,
                shareType = 'V'
            );
            
        
        //Turn trigger off with Run variable from code
        ContentDocumentLinkHandler.run=false;

        Test.startTest();
        insert cdlInsert;
        Test.stopTest();
        
        ContentDocumentLinkHandler.run=true;
                

        //Requery Data for Assertions
        List<IDP_Document__c> testIDPDocumentList = [select Id, Name, Policy__c, Policy_Number__c, Policy__r.PL_Policy_Number__c, Source__c, Document_Type__c
                                                     from IDP_Document__c];
        ContentDocumentLink testCDL = [select Id, ContentDocumentId, ContentDocument.Title, LinkedEntityId 
                                       from ContentDocumentLink 
                                       where ContentDocumentId = :cd.Id
                                       and LinkedEntityId != :UserInfo.getUserId()
                                       limit 1
                                      ]; //should be 1 anyway
        
        //Assertions
        
        //File is not moved and no custom Document record is created for it
        //Meaning: ContentDocumentLink.LinkedEntityId = AssetId
        System.assertEquals('02i', String.valueOf(testCDL.LinkedEntityId).substring(0,3)); 
        System.assertEquals(0, testIDPDocumentList.size());
        
        //File attached to Asset is not moved to a custom Document record. 
        System.assertNotEquals('aCm', String.valueOf(testCDL.LinkedEntityId).substring(0,3)); 
        
  
    }
    @isTest
    static void TriggerIsEnabledTest() {
        System.assert(PLUtilities.getIsTriggerEnabled('A nonsense phrase that cannot be a trigger name should return as true'));
        System.assert(PLUtilities.getIsTriggerEnabled('EnabledScenario_ForTestClass'));
        System.assertEquals(false,PLUtilities.getIsTriggerEnabled('DisabledScenario_ForTestClass'));
    }    
    
}