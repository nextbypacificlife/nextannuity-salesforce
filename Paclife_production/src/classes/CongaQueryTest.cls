/* This class requires SeeAllData=true, so that the existing Conga Queries can be tested to see if they will compile.
 * No assertions are needed. If the query executes without error, that is the proof that we need to make sure they are correct and the required objects and fields exist in the current environment.
 * This test class is unbulkified. That is the only way it will work, as we need to test each query. 
 * If in the future we have more than 90 queries, we will need to copy the test method into multiple identical ones, then just change the initial query to get from record 91 to 180, 181 to 270, etc.
 * Using 90 records to steer clear of the 100 SOQL query limit.
 */
@isTest(SeeAllData=true)
public class CongaQueryTest {

    @isTest
    static void CongaQueriesCompile() {
        
        //Get up to 90 Conga queries, to avoid hitting the 101 SOQL queries governor limit
        //If in a future implementation there are more than 90 queries, will need to split into multiple test methods
        List<APXTConga4__Conga_Merge_Query__c> congaQueryList = [select id, APXTConga4__Name__c, APXTConga4__Query__c
                                                                from APXTConga4__Conga_Merge_Query__c LIMIT 90];
        if (congaQueryList.size()>0){
            
            for(APXTConga4__Conga_Merge_Query__c cq : congaQueryList){
                String queryString = cq.APXTConga4__Query__c;
                System.debug(queryString);
                
                //TODO: Put this parsing logic in utility class
                Integer lastParameterStartPosition=-1;
                String parameterOpenChar='\'{';
                Integer lastParameterEndPosition=-1;
                String parameterCloseChar='}\'';
                String replacementParameter='\'\'';
                while(queryString.contains(parameterOpenChar)){
                    lastParameterStartPosition = queryString.indexOf(parameterOpenChar, lastParameterEndPosition+1);
                    lastParameterEndPosition = queryString.indexOf(parameterCloseChar, lastParameterStartPosition+1);
                    System.debug('start='+lastParameterStartPosition);
                    System.debug('end='+lastParameterEndPosition);
                    String queryParameter = queryString.substring(lastParameterStartPosition,lastParameterEndPosition+parameterCloseChar.length());
                    System.debug(queryParameter);
                    queryString=queryString.replace(queryParameter, replacementParameter);
                }
                
                String limitString = ' LIMIT ';
                String limitZero = ' LIMIT 0';
                if (querystring.toUpperCase().contains(limitString)){
                    queryString = querystring.left(querystring.indexof(limitString))+limitZero;
                } else {
                    queryString = queryString + limitZero;
                }
                
                //System.debug('unescaped cleansed query:  '+queryString);
                //queryString = String.escapeSingleQuotes(queryString);
                System.debug('final query:  '+queryString);
                //Yes this is unbulkified, but that is the only way this will work
                //If we hit the limits, will need to break into multiple similar test methods
                List<sObject> sobjList = Database.query(queryString); 
                
            }
            
        }
        
    }
}