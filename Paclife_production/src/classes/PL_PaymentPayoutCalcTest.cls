@isTest()
public class PL_PaymentPayoutCalcTest{

  @isTest static void testInvalidEvent() {
      
       Map<String,Id> recordTypeMap = new Map<String,Id>();
        for(RecordType rt : [select Id, SobjectType, DeveloperName from RecordType where DeveloperName in ('PersonAccount','IndividualAnnuity','Carrier')]){
            recordTypeMap.put(rt.SobjectType+'||'+rt.DeveloperName,rt.Id);
        }
        
      Account testaccount = new Account(RecordTypeId=recordTypeMap.get('Account||PersonAccount'),LastName='TEST Account 1',vlocity_ins__SocialSecurityNumber__pc='123.45.6677');
      insert testaccount;
      
      Account testaccount2 = new Account(RecordTypeId=recordTypeMap.get('Account||Carrier'), Name='TEST Account 1');
      insert testaccount2;
      
      contact ct=new contact(OtherStreet='test',OtherPostalCode='98001',OtherCity ='New york',OtherState ='CA',AccountId=testaccount2.Id, LastName='testnm',email='test@test.com');
      insert ct;
      
      Asset assetRec = new Asset(
                Name='TEST Policy 1',
                AccountId=testaccount.Id,
                PL_Policy_Number__c='PLN-100',
                IDP_Status__c='Application Upload',
                Status='Application' 
            );
        
        insert assetRec;
      
     
     
     vlocity_ins__Party__c vp=new vlocity_ins__Party__c();
     vp.vlocity_ins__AccountId__c=testaccount.Id;
     vp.vlocity_ins__ContactId__c=ct.Id;
     insert vp;
     
     vlocity_ins__PartyRelationshipType__c vrt = new vlocity_ins__PartyRelationshipType__c ();
     vrt.Name = 'Primary Annuitant';
     vrt.vlocity_ins__Color__c ='Blue';
     vrt.vlocity_ins__SourceString__c = 'Source';
     vrt.vlocity_ins__TargetRole__c= 'Company';
     insert vrt;
     
     vlocity_ins__AssetPartyRelationship__c vap=new vlocity_ins__AssetPartyRelationship__c();
     vap.vlocity_ins__PartyId__c=vp.Id;
     vap.vlocity_ins__PartyRelationshipTypeId__c= vrt.id;
     vap.vlocity_ins__AssetId__c=assetRec.Id;
     insert vap;
     
     List<vlocity_ins__Party__c> party = new List<vlocity_ins__Party__c>();
     party = [Select id from vlocity_ins__Party__c where vlocity_ins__AccountId__c =: testaccount.Id] ;
     system.debug('party'+party);
     Map<String,Object> firstMap = new Map<String,Object>();
     firstMap.put('ins_PartyType','Individual');
     firstMap.put('idp_PartyId',vp.Id);
     if(party.size()>0)
        firstMap.put('idp_PartyId',party[0].Id);
     firstMap.put('idp_InsRole','Primary Annuitant');
    
     List<Map<String,Object>> LstFirstMap = new List<Map<String,Object>>();
         
     LstFirstMap.add(firstMap);
         
     Map<String,Object> anoterMap = new Map<String,Object>();
     anoterMap.put('Annuity Party',LstFirstMap);
         
     Object objecHere = (Object)anoterMap;
         
     Map<String,Object>  finalMap = new Map<String,Object>();
     finalMap.put('insuredItems',objecHere);
         
     Map<String, Object> inpMap = new Map<String, Object>();
         
     inpMap.put('AssetId',assetRec.Id);
     inpMap.put('insuredItems',objecHere);
        
     Map<String,Object> outMap = new Map<String,Object>();
     Map<String,Object> options1 = new Map<String,Object>();
    
     createPartyRelationship controller = new createPartyRelationship();
     controller.invokeMethod('createHeldPartyRec',inpMap,outMap,options1);
    
     System.assert(controller.invokeMethod('createHeldPartyRec',inpMap,outMap,options1) == true);

          /*List<vlocity_ins__AssetPartyRelationship__c> heldPartyObjList = [select Id,Name from 
                                                                                        vlocity_ins__AssetPartyRelationship__c where 
                                                               vlocity_ins__AssetId__c =:assetRec.Id AND vlocity_ins__partyId__c =:data.Id];*/
                                                               
      List<vlocity_ins__AssetPartyRelationship__c> data= new List<vlocity_ins__AssetPartyRelationship__c>();
      data=[SELECT Id, Name,vlocity_ins__PartyRelationshipTypeId__r.Name FROM vlocity_ins__AssetPartyRelationship__c where Name='Primary Annuitant'] ;   
      system.debug('data'+data); 
    

       System.savepoint sp = Database.setSavepoint();
       String methodName = 'isValidPayoutStartDate';

        Map<String, Object> inputs = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        Map<String, Object> options = new Map<String, Object>();
        inputs.put('arguments',new List<Object>{assetRec.Id});

        PL_PaymentPayoutCalc calc = new PL_PaymentPayoutCalc();
        calc.invokeMethod(methodName, inputs, output, options);
        Database.rollback(sp);

}
}