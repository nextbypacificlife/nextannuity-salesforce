public class EmailSendController {
 @AuraEnabled 
    public static void sendMailMethod(String myMail, String mySubject, String myBody, String myPhone, String myName){
    
     //String EmailBody = ' Name ' + myName + '\n' + 'Phone ' + myPhone + '\n' + 'Subject ' + mySubject + '\n' + myBody;
        
     List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();     
  
     // Step 1: Create a new Email
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
    // Step 2: Set list of people who should get the email
       List<String> sendTo = new List<String>();
       //sendTo.add('anthony.richards21@nttdata.com');
       //sendTo.add('bobbyhoque@gmail.com');

       sendTo.add('hello@meetnext.com');
       mail.setToAddresses(sendTo);
    
    // Step 3: Set who the email is sent from
       mail.setReplyTo('noreply@meetnext.com'); // change it with your mail address.
       mail.setSenderDisplayName('Web Contact Form'); 
    
    // Step 4. Set email contents - you can use variables!
       String EmailBody = '<html><body>Name: ' + myName + '<br>' + 'Phone: ' + myPhone + '<br>' + 'Email: ' + myMail + '<br>' + myBody + '</body></html>';

      mail.setSubject('Web Contact Form ' + myName);
      mail.setHtmlBody(EmailBody);
    
    // Step 5. Add your email to the master list
      mails.add(mail);
    
  // Step 6: Send all emails in the master list
     Messaging.sendEmail(mails);
   }   
}