@isTest
private class IDP_CustomVlocityFunctionsTest {
    
    @isTest static void test_listjoinbykey() {
        Map<String, Object> inputs = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        List<Object> arguments = new List<Object> {
                new Map<String, Object> { 'VLOCITY-FORMULA-LIST' => new List<Object>() },
                'ruleDetails:message',
                '\n'
            };
            inputs.put('arguments', arguments);
        new IDP_CustomVlocityFunctions().invokeMethod('listjoinbykey', inputs, output, null);
    }
    
    @isTest static void test_getlinebreak() {
        Map<String, Object> inputs = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        new IDP_CustomVlocityFunctions().invokeMethod('getlinebreak', inputs, output, null);
    }
    
    @isTest static void test_listmergeunique() {
        Map<String, Object> inputs = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        
         //Assign record type map
        Map<String,Id> recordTypeMap = new Map<String,Id>();
        for(RecordType rt : [select Id, SobjectType, DeveloperName from RecordType where DeveloperName in ('PersonAccount','IndividualAnnuity')]){
            recordTypeMap.put(rt.SobjectType+'||'+rt.DeveloperName,rt.Id);
        }
        
        //Account
        List<Account> accountInsertList = new List<Account>();
        for (Integer i=0; i<1; i++) {
            accountInsertList.add(new Account(
                RecordTypeId=recordTypeMap.get('Account||PersonAccount'),
                LastName='TEST Account '+String.valueOf(i),vlocity_ins__SocialSecurityNumber__pc = '123-45-678'+String.valueOf(i)
            ));
        }
        insert accountInsertList;
                
        //Asset/Policy
        List<Asset> assetInsertList = new List<Asset>();
        for (Integer i=0; i<1; i++) {
            assetInsertList.add(new Asset(
                RecordTypeId=recordTypeMap.get('Asset||IndividualAnnuity'),
                Name='TEST Policy '+String.valueOf(i),
                AccountId=accountInsertList[Math.mod(i,5)].Id,
                PL_Policy_Number__c='PLN'+String.valueOf(i*13+100), //unique while decidedly different from the name values
                IDP_Status__c='Application Upload', 
                Status='Application' 
            ));
        }
        //insert assetInsertList;
        
        //List<Asset> testAssetList = [select Id,IDP_Status__c, Name from Asset];
        //testAssetList[0].IDP_Status__c='IGO/Suitability Failed';
        //update testAssetList;
        Try{
            

            List<Object> arguments = new List<Object> {
                'key1',
                'key1',
                new Map<String, Object> { 'VLOCITY-FORMULA-LIST' => new List<Object>() },
                new Map<String, Object> { 'VLOCITY-FORMULA-LIST' => new List<Object>() } 
            };
            inputs.put('arguments', arguments);
            new IDP_CustomVlocityFunctions().invokeMethod('listmergeunique', inputs, output, null);
            // test error for code coverage
            new IDP_CustomVlocityFunctions().invokeMethod('listmergeunique', new Map<String, Object>(), output, null);
            
        }catch(Exception e){}
    }
    
}