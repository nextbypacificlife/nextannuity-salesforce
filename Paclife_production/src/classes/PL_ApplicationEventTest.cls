@isTest
public class PL_ApplicationEventTest {
    @isTest static void testInvalidEvent() {
        // Missing required field (Event_Type__c)
        Contract_Event__e appEvent = new Contract_Event__e(Contract_Id__c='02iV0000006dV4BIAU',
                                             Contract_Number__c = 'XP1810031', 
                                             Description__c='Application created successfully.'
                                        );

        Test.startTest();
        Database.SaveResult sr = EventBus.publish(appEvent);
        Test.stopTest();

        System.assertEquals(false, sr.isSuccess());
    }
    
    @isTest static void testValidEvent() {
        
        // Create a test event instance
        Contract_Event__e appEvent = new Contract_Event__e(Contract_Id__c='02iV0000006dV4BIAU',
                                             Contract_Number__c = 'XP1810031',                   
                                             Event_Type__c ='Created',
                                             Description__c='Application created successfully.'
                                        );
        
        Test.startTest();
        
        // Publish test event
        Database.SaveResult sr = EventBus.publish(appEvent);
            
        Test.stopTest();
                
        // Perform validations here
        
        // Verify SaveResult value
        System.assertEquals(true, sr.isSuccess());
    }
}