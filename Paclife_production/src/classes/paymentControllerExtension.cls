public with sharing class paymentControllerExtension {
     
    IDP_Payment__c paym;
    ApexPages.StandardController sController;
    public boolean isError{get;set;}
 
    public paymentControllerExtension (ApexPages.StandardController controller) {
        sController = controller;
        paym = (IDP_Payment__c)controller.getRecord();
        System.debug('paym debug'+paym);
    }
     
    public PageReference save() {
    PageReference pr = new PageReference('/'+paym.id);
    paym=[select status__c from IDP_Payment__c where id=:paym.id ][0];
    
    if(paym.Status__c == 'Successful'){
        Map<String, Object> input;
        input = new Map<String, Object> {
                    'Id' =>paym.id
                };
        System.debug('Input' + input );        
        Map<String, Object> options = new Map<String, Object>();                
        Map<String,Object> IntegrationOutput = new Map<String,Object>();
                   
        Object result = (new vlocity_ins.IntegrationProcedureService()).invokeMethod('PL_Contract_CallInitialPaymentService', input, IntegrationOutput , options);
        System.debug('--Output: ' + IntegrationOutput ); 
        update paym;
        pr.setRedirect(true);
        return pr;
    }else
       {
          System.debug('not success');
           isError=true;
           ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'You can not retrieve the payment information untill Payment is Successful.'));        
           return null;
    }
    }
    public PageReference AddErrorMessage(){
       PageReference pg = new PageReference('/'+paym.id);
       pg.setRedirect(true);
       return pg;
     }  
}