@isTest
private class createPartyRelationshipTest {
    
    
    private static testMethod void test1() {
        
         //Assign record type map
        Map<String,Id> recordTypeMap = new Map<String,Id>();
        for(RecordType rt : [select Id, SobjectType, DeveloperName from RecordType where DeveloperName in ('PersonAccount','IndividualAnnuity')]){
            recordTypeMap.put(rt.SobjectType+'||'+rt.DeveloperName,rt.Id);
        }
        
        //Account
       List<Account> accListToBeAdded = new List<Account>();
       Account testaccount = new Account(RecordTypeId=recordTypeMap.get('Account||PersonAccount'),LastName='TEST Account 1',vlocity_ins__SocialSecurityNumber__pc='123.45.6677');
       Account testaccount1 = new Account(RecordTypeId=recordTypeMap.get('Account||PersonAccount'),LastName='TEST Account 2',vlocity_ins__SocialSecurityNumber__pc='123.45.6688');
       accListToBeAdded.add(testaccount);
       accListToBeAdded.add(testaccount1);
       
       insert accListToBeAdded;
                
       //Asset/Policy
         Asset assetRec = new Asset(
                RecordTypeId=recordTypeMap.get('Asset||IndividualAnnuity'),
                Name='TEST Policy 1',
                AccountId=testaccount.Id,
                PL_Policy_Number__c='PLN-100',
                IDP_Status__c='Application Upload   ',
                Status='Application' 
            );
        
        insert assetRec;
        
        List<vlocity_ins__Party__c> partyRecsTobeAdded = new List<vlocity_ins__Party__c>();
        vlocity_ins__Party__c partyRec = new vlocity_ins__Party__c(Name = 'Test Party 1',vlocity_ins__AccountId__c = testaccount.Id,vlocity_ins__PartyEntityType__c = 'Account');
        vlocity_ins__Party__c partyRec1 = new vlocity_ins__Party__c(Name = 'Test Party 2',vlocity_ins__AccountId__c = testaccount1.Id,vlocity_ins__PartyEntityType__c = 'Account');
        partyRecsTobeAdded.add(partyRec);
        partyRecsTobeAdded.add(partyRec1);
        
        insert partyRecsTobeAdded;
        
        Map<String,Object> firstMap = new Map<String,Object>();
                            firstMap.put('ins_PartyType','Individual');
                            firstMap.put('idp_PartyId',partyRec.Id);
                            firstMap.put('idp_InsRole','Primary Owner');
                            firstMap.put('idp_InsBeneficaryPercentage','72');
                            firstMap.put('idp_InsSign','true');
                            firstMap.put('idp_InsSignLocation','NY');
                            firstMap.put('idp_InsSignDate','09/09/2009');
        Map<String,Object> secMap = new Map<String,Object>();
                            secMap.put('ins_PartyType','Individual');
                            secMap.put('idp_PartyId',partyRec1.Id);
                            secMap.put('idp_InsRole','Beneficiary');
          
        List<Map<String,Object>> LstFirstMap = new List<Map<String,Object>>();
         
        LstFirstMap.add(firstMap); LstFirstMap.add(secMap);
         
        Map<String,Object> anoterMap = new Map<String,Object>();
        anoterMap.put('Annuity Party',LstFirstMap);
         
        Object objecHere = (Object)anoterMap;
         
        Map<String,Object>  finalMap = new Map<String,Object>();
        finalMap.put('insuredItems',objecHere);
         
        Map<String, Object> inpMap = new Map<String, Object>();
         
        inpMap.put('AssetId',assetRec.Id);
        inpMap.put('insuredItems',objecHere);
        
        Map<String,Object> outMap = new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        
        createPartyRelationship controller = new createPartyRelationship();
        controller.invokeMethod('createHeldPartyRec',inpMap,outMap,options);
        System.assert(controller.invokeMethod('createHeldPartyRec',inpMap,outMap,options) == true);
          
    }
    
    

}