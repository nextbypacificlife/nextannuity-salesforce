public class AccountTriggerHandler {
    //public class customException extends Exception {}
    //static RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
    Static Id AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
    
    public static void update_ssn_lastfour_digits(){
        List<account> accountsToInsert = (List<account>)trigger.new;
        //Deteremine if this a person record - and if so perform person record updates
        Id latestrecordId;
        for(Account acc : accountsToInsert){
            if(acc.recordtypeId == AccRecordTypeId ){
                try{ 
                    latestrecordId=acc.Id;
                    if(acc.vlocity_ins__SocialSecurityNumber__pc!=null)
                       acc.PL_AccessCode__pc = string.valueOf(acc.vlocity_ins__SocialSecurityNumber__pc).right(4);
                }
                catch(Exception e) {
                    Trigger.newMap.get(latestrecordId).addError('Unable to udpate Last 4 of SSN field');
                }
            }
        }   
    }
}