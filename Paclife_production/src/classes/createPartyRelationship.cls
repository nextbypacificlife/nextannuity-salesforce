global class createPartyRelationship implements vlocity_ins.VlocityOpenInterface{
 global Boolean invokeMethod(String methodName, 
                               Map<String,Object> inputMap, 
                               Map<String,Object> outputMap, 
                               Map<String,Object> options) 
   {
           if(methodName=='createHeldPartyRec'){
               
    //System.debug('**inputMap**: '+inputMap);
               
               List<vlocity_ins__AssetPartyRelationship__c> contractRelListToBeAdded = new List<vlocity_ins__AssetPartyRelationship__c>();
               Set<String> roleSet = new Set<String>();
               Map<String,ID> roleToRelTypeId = new Map<String,Id>();
               Map<String,vlocity_ins__PartyRelationshipType__c> roleToRelType = new Map<String,vlocity_ins__PartyRelationshipType__c>();
               List<Map<String, Object>> insuredItemsMap = new List<Map<String, Object>>();
               List<Object> insItemsMap = new List<Object>();
               
               String assetId = (String)inputMap.get('AssetId');
               
               Map<String,Object> filter = (Map<String,Object>)inputMap.get('insuredItems');
    
               
               if(inputMap.get('insuredItems')!=null){
                   
                   
                   String jsonInsMap = JSON.serialize(filter);
                   Map<String, Object> filterMap = (Map<String, Object>) JSON.deserializeUntyped(jsonInsMap);
                   for(String key:filterMap.keyset()){
                       insItemsMap = (List<Object>)filterMap.get(key);
                   }
                   
                   for (Object obj : insItemsMap) {
                       insuredItemsMap.add((Map<String, Object>)obj);
                   }
                   
                   for(Map<String,Object> mapList:insuredItemsMap){
                       for(String str:mapList.keyset()){
                           if(str.toLowerCase() == 'idp_InsRole'.toLowerCase()){
                               roleSet.add((String)mapList.get(str));
                           }
                       }
                   }
                   //Query to collect party relationship types Id
                   List<vlocity_ins__PartyRelationshipType__c> partyRelList = [Select Id,Name,vlocity_ins__TargetRole__c,vlocity_ins__SourcePartyType__c from vlocity_ins__PartyRelationshipType__c where Name in:roleSet and vlocity_ins__TargetRole__c in:roleSet];
    
                   System.debug('partyRelList'+partyRelList); 
                   for(vlocity_ins__PartyRelationshipType__c obj:partyRelList){
                       if(!roleToRelTypeId.containskey(obj.vlocity_ins__SourcePartyType__c)){
                           roleToRelTypeId.put(obj.Name,obj.Id);
                           roleToRelType.put(obj.Name,obj);
                       }
                   }

    
                   for(Map<String,Object> mapList:insuredItemsMap){
                       vlocity_ins__AssetPartyRelationship__c partyRelObj= new vlocity_ins__AssetPartyRelationship__c(vlocity_ins__AssetId__c = assetId);
                       String nameStr=assetId;

      
                       Boolean isPrimaryOwner = false;
                       String role = (String)mapList.get('idp_InsRole');
                       if(mapList.containskey('idp_InsRole') && role.toLowerCase() == 'Primary Owner'.toLowerCase()){
                           isPrimaryOwner = true;
                       }
                       
                       for(String str:mapList.keyset()){
                           
                           if(str.toLowerCase() == 'idp_PartyId'.toLowerCase()){
                               nameStr+=mapList.get(str);
                               partyRelObj.put('vlocity_ins__PartyId__c',mapList.get(str));
                           }
    
                           if(str.toLowerCase() == 'idp_InsRole'.toLowerCase()){
                               nameStr+=mapList.get(str);
                               Id partyRelId;
                               if(roleToRelTypeId.containsKey((String)mapList.get(str))){
                                   partyRelId = roleToRelTypeId.get((String)mapList.get(str));
                               }
                               if(partyRelId!=null){
                                   partyRelObj.put('vlocity_ins__PartyRelationshipTypeId__c',partyRelId);
                                   partyRelObj.vlocity_ins__PartyRelationshipTypeId__r = roleToRelType.get((String)mapList.get(str));
                                   partyRelObj.vlocity_ins__PartyRelationshipTypeId__r.Name = roleToRelType.get((String)mapList.get(str)).Name;
                                   System.debug('**partyRelObj:'+partyRelObj);
                                   System.debug('**partyRelObj Name :'+partyRelObj.vlocity_ins__PartyRelationshipTypeId__r.Name);
                                   System.debug('**partyRelObj LookupObj :'+partyRelObj.vlocity_ins__PartyRelationshipTypeId__r);
                               }
                           }

   
                           if(isPrimaryOwner){
                               if(str.toLowerCase() == 'idp_InsSign'.toLowerCase()){
                                  String temp = (String)mapList.get(str);
                                  Boolean isSigned = false;
                                  if(temp!=null) {
                                    isSigned = Boolean.valueOf(temp);
                                  }
    
                                   partyRelObj.put('IDP_Signature__c',isSigned);
                               }
          
                               if(str.toLowerCase() == 'idp_InsSignDate'.toLowerCase()){
                                  String temp = (String)mapList.get(str);
                                  Date signedDate;
                                  if(temp!=null) {
                                    signedDate = Date.parse(temp);
                                  }
    
                                   partyRelObj.put('IDP_SignatureDate__c', signedDate);
                               }
        
                               if(str.toLowerCase() == 'idp_InsSignLocation'.toLowerCase()){
                                   partyRelObj.put('IDP_SignatureState__c',mapList.get(str));
                               }
                           }
                           

                           if(str.toLowerCase() == 'idp_InsBeneficaryPercentage'.toLowerCase()){
                               Decimal strToDec = decimal.valueOf((String)mapList.get(str));
                               partyRelObj.put('vlocity_ins__BeneficiaryPercentage__c',strToDec);
                           }
                       }
                       partyRelObj.Name = nameStr;
                       contractRelListToBeAdded.add(partyRelObj);
                   }
                  
               }
               
               if(contractRelListToBeAdded.size()>0){
                   insert contractRelListToBeAdded;
               }
             System.debug('**After Insert contractRelListToBeAdded:'+contractRelListToBeAdded);
               
               //Populating PolicyNum in the outputMap
               if(assetId !=null){
                   Asset assetObj = [Select PL_Policy_Number__c FROM Asset where id=:assetId Limit 1];
                   if(assetObj !=null && assetObj.PL_Policy_Number__c !=null){
                       outputMap.put('contractId',assetObj.PL_Policy_Number__c);
                   }
               }
           }
           
           return true;
      }
}