@isTest
public class AccountTriggerHandlerTest {

static RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
    
    static testMethod void TestAccount() {

        Account a = new Account();
        a.FirstName = 'Fred';
        a.LastName = 'Smith';
        a.RecordType = personAccountRecordType;
        a.vlocity_ins__SocialSecurityNumber__pc = '123-45-6789';
        insert a;
    
        // Retrieve record inserted
        Account ac = new Account();
        ac=[SELECT Id,vlocity_ins__SocialSecurityNumber__pc,FirstName,LastName,Last4_SSN__pc  
            FROM Account WHERE Id =:a.Id];

        // Test that the trigger correctly inserted
        System.assertEquals('Fred', ac.FirstName);
        System.assertEquals('Smith', ac.LastName);
        ac.vlocity_ins__SocialSecurityNumber__pc = '123-55-9876';
        update ac;
      
    }
    
    static testMethod void TestAccountexception() {
    
        Try{
          Account ac = new Account();
          ac.FirstName = 'Fred';
          ac.LastName = 'Smith';
          ac.RecordType = personAccountRecordType;
          insert ac;
          
          // Retrieve record inserted
          Account acc = new Account();
          acc=[SELECT Id,PersonEmail,vlocity_ins__SocialSecurityNumber__pc,FirstName,LastName,Last4_SSN__pc  
              FROM Account WHERE Id =:ac.Id];
          acc.PersonEmail='testg@test.com';
          update acc;
        
        }
        catch(Exception e) {}
      
    }
        
}