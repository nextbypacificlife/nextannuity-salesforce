global class PLUtilities {
         
    global static sObject getObject(String typeName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            return null;
        }
        
        // Instantiate an sObject with the type passed in as an argument
        //  at run time.
        return targetType.newSObject(); 
    }

    //Convert Set of included object names to a Set of key prefixes
    global static Set<String> getObjectTypeSet(Set<String> affectedObjectSet) {
        Set<String> returnObjectTypeSet = new Set<String>();
        for(String includedObjectName : affectedObjectSet) {
            returnObjectTypeSet.add(PLUtilities.getObject(includedObjectName).getSObjectType().getDescribe().getKeyPrefix());
        }
        return returnObjectTypeSet;
    }
  
    //Convert Set of included object names to a Map of object API names to key prefixes
    global static Map<String,String> objectTypeMap = new Map<String,String>();
    global static void putInObjectTypeMap(String objectName) {
        if (PLUtilities.getObject(objectName) != null) {
            objectTypeMap.put(objectName, PLUtilities.getObject(objectName).getSObjectType().getDescribe().getKeyPrefix());            
        }
    }
    
	private static Map<String, Boolean> triggerSettingsMap = new Map<String, Boolean>(); 
	global static Boolean getIsTriggerEnabled(String triggerName){
		if(triggerSettingsMap.size() == 0){
			List<Trigger__mdt> triggerList = [select QualifiedApiName, Disabled__c FROM Trigger__mdt];
            for(Trigger__mdt t : triggerList) {
                triggerSettingsMap.put(t.QualifiedApiName, t.Disabled__c);
            }
		}
		
        Boolean triggerEnabled;
        
        if(triggerSettingsMap.get(triggerName) != null) {
            triggerEnabled = !triggerSettingsMap.get(triggerName);
        } else {
            triggerEnabled = true;            
        }
		
		return triggerEnabled;
	}    
}