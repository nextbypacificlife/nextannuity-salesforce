global with sharing class PL_PaymentPayoutCalc implements vlocity_ins.VlocityOpenInterface
{
  public Integer FIRST_ITEM_INDEX = 0;
  public Integer YEARS_TO_NINTIETH_BIRTHDATE = 90;
  public Integer YEARS_TO_ANNUITY_MATURITY = 30;

  public Id assetId;
  public Asset asset;
  public List<vlocity_ins__AssetPartyRelationship__c> partyRelations;

  public Boolean invokeMethod(String methodName, Map<String, Object> inputs, Map<String, Object> output, Map<String, Object> options)
  {
    if(methodName == 'isValidPayoutStartDate')
    {
      Boolean isInvalidPayoutStartDate = true;

      setVariableInstances(inputs);
      isInvalidPayoutStartDate = hasInvalidPayoutStartDate();

      output.put('result', isInvalidPayoutStartDate);
    }

    return true;
  }

  public void setVariableInstances(Map<String, Object> inputs) {
    List<Object> arguments = (List<Object>)inputs.get('arguments');
    setAssetAndAnnuitants(arguments);
  }

  public void setAssetAndAnnuitants(List<Object> arguments) {
    try{
       assetId = (Id)arguments[0];

      // Set party relations
      
      partyRelations = [SELECT Id, vlocity_ins__PartyId__r.vlocity_ins__ContactId__r.Birthdate, vlocity_ins__AssetId__r.CreatedDate,  vlocity_ins__AssetId__r.Annuity_Payment_Start_Date__c
      FROM vlocity_ins__AssetPartyRelationship__c WHERE vlocity_ins__PartyRelationshipTypeId__r.Name IN('Primary Annuitant','Joint Annuitant') AND vlocity_ins__AssetId__c =:assetId 
      Order By vlocity_ins__PartyId__r.vlocity_ins__ContactId__r.Birthdate ASC];

      //Set asset object instance
        if(partyRelations!=null )
           {
            asset = partyRelations[0].vlocity_ins__AssetId__r;}

    }catch (Exception ex) {
        system.debug(ex.getMessage()+'stack'+ex.getStackTraceString());
    }
  }

  public Boolean hasInvalidPayoutStartDate() {
    Boolean isInvalid = true;

    if(asset == null)return isInvalid;

    try{

      Date annuityPayoutStartDate = getAnnuityPayoutDate();

      Date earliestPayoutLimit = getEarliestPayoutLimit();

      if(annuityPayoutStartDate != null && earliestPayoutLimit != null)
        isInvalid = annuityPayoutStartDate > earliestPayoutLimit;

    }catch (Exception ex) {
        system.debug(ex.getMessage());
    }


    return isInvalid;
  }

  public Date getAnnuityPayoutDate() {
    if(asset==null)return null;
    return asset.Annuity_Payment_Start_Date__c;
  }

  public Date getEarliestPayoutLimit() {
    Date ret;
    Date appReceivedDate = getAppReceivedDate();
    Date matureDate = appReceivedDate.addYears(YEARS_TO_ANNUITY_MATURITY); 
    Date earliestAnnuitantBirthDate = getEarliestAnnuitantBirthDate();
    Date ninetyBirthDate;

    if(earliestAnnuitantBirthDate != null)
      ninetyBirthDate = earliestAnnuitantBirthDate.addYears(YEARS_TO_NINTIETH_BIRTHDATE);
       // system.debug('@ninetyBirthDate:'+ninetyBirthDate);

    return ((ninetyBirthDate==null||matureDate < ninetyBirthDate)?matureDate:ninetyBirthDate);
  }

  public Date getAppReceivedDate() {
    if(asset==null)return null;
    return Date.valueOf(asset.CreatedDate);
  }

  public Date getEarliestAnnuitantBirthDate(){
    Date ret;

    if(partyRelations == null || partyRelations.isEmpty())return ret;
    
    for(vlocity_ins__AssetPartyRelationship__c partyRelation : partyRelations) {
      if(partyRelation.vlocity_ins__PartyId__r.vlocity_ins__ContactId__r.Birthdate==null)continue;

      if(ret==null || 
        partyRelation.vlocity_ins__PartyId__r.vlocity_ins__ContactId__r.Birthdate < ret){

        ret = partyRelation.vlocity_ins__PartyId__r.vlocity_ins__ContactId__r.Birthdate;        
      }
        
    }
  
    return ret;
  }
}