trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {

    if(Trigger.isAfter && Trigger.isInsert) {
        ContentDocumentLinkHandler.MoveToCustomRepository(Trigger.new); 
    }
}