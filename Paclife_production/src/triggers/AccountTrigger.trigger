Trigger AccountTrigger on Account (before insert, before update){
    
    Boolean run = true; 
    if(!(run && PLUtilities.getIsTriggerEnabled('AccountTrigger'))){
         return;
    }
    if(Trigger.IsBefore)
    {
       if(Trigger.IsInsert || Trigger.IsUpdate){
          AccountTriggerHandler.update_ssn_lastfour_digits();
       }
    }
    
}