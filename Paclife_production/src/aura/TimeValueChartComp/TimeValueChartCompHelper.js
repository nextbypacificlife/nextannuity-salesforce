({
	    createLineGraph : function(cmp, temp, monthly) {
        
        var label = [];
        var firstValue = [];
        var nYears = 16;
        var runningPeriodValue = monthly;    
            
        for( var n = 0; n < nYears; n++ )
        {
        	runningPeriodValue =  ((runningPeriodValue * 1.12).toFixed(2));   // 1.12 is 4% per year for 3 years 12 %..
            firstValue.push(runningPeriodValue); 
        }  
        
        for(var a=0; a< temp.length; a++){
            console.debug(temp[a]["label"]);
            label.push(temp[a]["label"]);
        }    

        var el = cmp.find('lineChart').getElement();
        //var ageSelector = cmp.find('ageSelect').getElement();
        var ctx = el.getContext('2d');
        
        new Chart(ctx, {
            type: 'line',
            data: {
                    labels: label,
                    datasets: [{
                      label: 'Time and Value Graph',
                      borderWidth: 2, 
                      data: firstValue,
                      lineWidth: 4,  
                      fill: false,
                      backgroundColor: "rgba(3,166,120,0.7)"
                    }
                  ]
             },
    	options: {
        scales: {
            	xAxes: [{
					gridLines: {
                	display:false
            	},                
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: runningPeriodValue
                }
            }],
                yAxes: [{   
                      ticks: {
                      	display: false
                      },                    
                      scaleLabel: {
                        display: true,
                        labelString: "Projected Monthly Income",
                        fontColor: "black"
                      }
                    }]            
                }
            }
        });
	}    
})