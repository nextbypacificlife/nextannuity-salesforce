({
    sendMail: function(component, event, helper) {
        // when user click on Send button 
        // First we get all 3 fields values 	
        var getName = component.get("v.personname");
        var getEmail = component.get("v.email");
        ///getEmail = "hello@meetnext.com";
        //getEmail = "anthony.richards21@nttdata.com";
        var getphone = component.get("v.phone");
        var getbody = component.get("v.body");
        
        // check if Email field is Empty or not contains @ so display a alert message 
        // otherwise call call and pass the fields value to helper method    
        if ($A.util.isEmpty(getEmail) || !getEmail.includes("@")) {
            alert('Please Enter valid Email Address');
        } else {
            helper.sendHelper(component, getName, getEmail, getphone, getbody);
        }
    },
  
    closeMessage: function(component, event, helper) {
        component.set("v.mailStatus", false);
        component.set("v.name", null);
        component.set("v.email", null);
        component.set("v.phone", null);
        component.set("v.body", null);
    },
})