({
    sendHelper: function(component, getName, getEmail, getphone, getbody, getSubject) {
        // call the server side controller method 	
        getSubject = "Welcome from MeetNext!";
        var action = component.get("c.sendMailMethod");
        // set the 3 params to sendMailMethod method   
        // 
        
        action.setParams({
            'myMail': getEmail,
            'mySubject': getSubject,
            'myPhone': getphone,
            'myBody': getbody,
            'myName': getName            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                // if state of server response is comes "SUCCESS",
                // display the success message box by set mailStatus attribute to true
                component.set("v.mailStatus", true);
            }
        });
        $A.enqueueAction(action);
    },
})


// (String myMail, String mySubject, String myBody, String myPhone, String myName)